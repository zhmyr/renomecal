import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// eslint-disable-next-line no-unused-vars
let store;
export default store = new Vuex.Store({
  state: {
    stage: 1,
    current: 1,
    conflict: [],
    num: 0,
    format: "",
    tiraj: 0,
    isbn: undefined,
    maket: {
      need: undefined,
      kvo: {
        type: -1,
        num: 0
      },
      tbl: 0,
      pic: 0,
      edit: undefined,
      cor: undefined
    },
    disign: 0,
    print: {
      init: false,
      fst: {
        color: "",
        paper: "",
        ro: -1
      },
      scd: {
        num: 0,
        color: "",
        paper: "",
        ro: -1
      }
    },
    manus: {
      type: -1
    },
    block: -1
  },
  getters: {
    sumList: state => {
      return Number.parseInt(state.num) + Number.parseInt(state.print.scd.num);
    },
    lock65: state => {
      return (
        ["100x140", "115x165", "145x215", "165x235", "205x290"].indexOf(
          state.format
        ) != -1 ||
        state.print.fst.color == "4+4" ||
        state.tiraj <= 100
      );
    },
    lockISBN: state => state.tiraj < 30,
    lockScob: (state, getters) => {
      if (state.print.fst.paper == 1 && [1, 2].indexOf(state.print.fst.ro) != -1 && getters.sumList > 96)
        return true;
      return state.hard !== undefined || getters.sumList > 64;
    },
    lockTerm: (state, getters) => state.hard !== undefined || getters.sumList < 39,
    lockTermNi: (state, getters) => getters.sumList < 48 && state.hard !== undefined,
    lockNi: (state, getters) => getters.sumList < 48 && state.soft !== undefined,
    lockHard: (state, getters) => getters.sumList < 48,
    lockRound: (state, getters) => getters.sumList < 300,
    lockKart: state => state.tiraj <= 100
  },
  mutations: {
    setStage(state, val) {
      if (val > state.stage) state.stage = val;
    },
    setCurrent(state, val) {
      state.current = val;
    },
    setFormat(state, val) {
      state.format = val;
      check65(state);
    },
    setTiraj(state, val) {
      state.tiraj = val;
      check65(state);
      if (state.soft !== undefined && state.soft.material == 3)
        this.commit("soft/setMaterial", [-1, state.tiraj]);
    },
    setISBN(state, val) {
      state.isbn = val;
    },
    setNeedMaket(state, val) {
      state.maket.need = val;
    },
    setKvoMaket(state, val) {
      state.maket.kvo.num = val;
      state.num = calcMaket(
        state.maket.kvo.type,
        val,
        state.maket.tbl,
        state.maket.pic,
        state.format
      );
    },
    setTypeMaket(state, val) {
      state.maket.kvo.type = val;
    },
    setTblMaket(state, val) {
      state.maket.tbl = val;
      state.num = calcMaket(
        state.maket.kvo.type,
        state.maket.kvo.num,
        val,
        state.maket.pic,
        state.format
      );
    },
    setPicMaket(state, val) {
      state.num = calcMaket(
        state.maket.kvo.type,
        state.maket.kvo.num,
        state.maket.tbl,
        val,
        state.format
      );
      state.maket.pic = val;
    },
    setEditMaket(state, val) {
      state.maket.edit = val;
    },
    setCorMaket(state, val) {
      state.maket.cor = val;
    },
    setDisign(state, val) {
      state.disign = val;
    },
    setNum(state, val) {
      while (val % 4 != 0) val++;
      state.num = val;
    },
    setFstColor(state, val) {
      state.print.fst.color = val;
      if (val == "4+4")
        if (
          ["100x140", "115x165", "145x215", "165x235", "205x290"].indexOf(
            state.format
          ) != -1 ||
          state.tiraj <= 100
        )
          this.commit("setFstRo", -1);
    },
    setFstPaper(state, val) {
      state.print.fst.paper = val;
    },
    setFstRo(state, val) {
      state.print.fst.ro = val;
    },
    setScd(state, val) {
      state.print.init = val;
    },
    setScdColor(state, val) {
      state.print.scd.color = val;
      if (val == "4+4")
        if (
          ["100x140", "115x165", "145x215", "165x235", "205x290"].indexOf(
            state.format
          ) != -1 ||
          state.tiraj <= 100
        )
          this.commit("setScdRo", -1);
    },
    setScdPaper(state, val) {
      state.print.scd.paper = val;
    },
    setScdRo(state, val) {
      state.print.scd.ro = val;
    },
    setScdNum(state, val) {
      while (val % 4 != 0) val++;
      state.print.scd.num = val;
    },
    setManusType(state, val) {
      state.manus.type = val;
    },
    setBlock(state, val) {
      state.block = val;
    }
  },
  actions: {}
});

function check65(state) {
  if (
    ["100x140", "115x165", "145x215", "165x235", "205x290"].indexOf(
      state.format
    ) != -1 ||
    state.print.fst.color == "4+4" ||
    state.tiraj <= 100
  ) {
    if (state.print.fst.ro == 1) this.commit("setFstRo", -1);
    if (state.print.scd.ro == 1) this.commit("setScdRo", -1);
  }
}

function calcMaket(type, val, tbl, pic, format) {
  let num =
    type == 3
      ? Number.parseInt(val)
      : Number.parseInt(val) *
        (1 + Number.parseInt(tbl) / 100) *
        (1 + Number.parseInt(pic) / 100);
  if (type == 1) {
    switch (format) {
      case "100x140":
        num /= 1000;
        break;
      case "115x165":
        num /= 1400;
        break;
      case "130x200":
        num /= 1800;
        break;
      case "140x200":
        num /= 2100;
        break;
      case "140x205":
        num /= 2100;
        break;
      case "145x215":
        num /= 2300;
        break;
      case "165x235":
        num /= 2600;
        break;
      case "205x290":
        num /= 3500;
        break;
      default:
        break;
    }
  } else if (type == 2) {
    switch (format) {
      case "100x140":
        num /= 22;
        break;
      case "115x165":
        num /= 28;
        break;
      case "130x200":
        num /= 32;
        break;
      case "140x200":
        num /= 34;
        break;
      case "140x205":
        num /= 34;
        break;
      case "145x215":
        num /= 38;
        break;
      case "165x235":
        num /= 42;
        break;
      case "205x290":
        num /= 48;
        break;
      default:
        break;
    }
  }
  num = Math.ceil(num);
  if (type != 3)
    if (num >= 1 && num <= 120) num += 4;
    else if (num > 120 && num <= 220) num += 5;
    else if (num > 220 && num <= 280) num += 6;
    else num += 8;
  while (num % 4 != 0) num++;
  return num;
}
