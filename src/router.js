import Vue from "vue";
import Router from "vue-router";
import store from "./store";

Vue.use(Router);
let path = [
  "format",
  "tiraj",
  "isbn",
  "maket",
  "disain",
  "print",
  "manus",
  "block",
  "final"
];

let router;
export default router = new Router({
  routes: [
    {
      path: "/",
      name: "format",
      component: () => import("./views/Format.vue")
    },
    {
      path: "/tir",
      name: "tiraj",
      component: () => import("./views/Tiraj.vue")
    },
    {
      path: "/isbn",
      name: "isbn",
      component: () => import("./views/ISBN.vue")
    },
    {
      path: "/maket",
      name: "maket",
      component: () => import("./views/Maket.vue")
    },
    {
      path: "/dis",
      name: "disain",
      component: () => import("./views/Disain.vue")
    },
    {
      path: "/print",
      name: "print",
      component: () => import("./views/Print.vue")
    },
    {
      path: "/man",
      name: "manus",
      component: () => import("./views/Manus.vue")
    },
    {
      path: "/blk",
      name: "block",
      component: () => import("./views/Block.vue")
    },
    {
      path: "/final",
      name: "final",
      component: () => import("./views/Final.vue")
    }
  ]
});
router.beforeEach((to, from, next) => {
  if (
    path.indexOf(from.name) < store.state.stage &&
    path.indexOf(to.name) < store.state.stage
  ) {
    store.commit("setCurrent", path.indexOf(to.name) + 1);
    next();
  } else if (store.state.stage == 1) next();
  else if (from.name === null) next();
});
