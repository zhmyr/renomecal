import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Title from "./components/Title.vue";
import SubTitle from "./components/SubTitle.vue";
import Page from "./components/Page.vue";
import Add from "./components/AddTitle.vue";
import Soft from "./components/SofrManus.vue";
import Hard from "./components/HardManus.vue";

Vue.config.productionTip = false;
Vue.component("soft", Soft);
Vue.component("hard", Hard);
Vue.component("renome-title", Title);
Vue.component("renome-page", Page);
Vue.component("renome-sub", SubTitle);
Vue.component("renome-add", Add);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
